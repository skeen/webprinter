WebPrinter
========

The goal of this project is to provide a minimal 3D printer webserver interface with Angular2.0 and Typescript.

## Install

Clone this repo and execute in your favourite shell:

* `npm i -g gulp` to install gulp globally
* `npm i` to install local npm dependencies

## Play

After completing installation type in your favourite shell:

* `gulp play` (or `npm start`) to compile the entire project, and setup a webserver hosting the result. Typescript files are being observed and will be re-transpiled on each change.

## Dependencies

### Build-time

* [Gulp](https://github.com/gulpjs/gulp): (Streaming) Build system
* [TypeScript](https://github.com/Microsoft/TypeScript): TypeScript to JavaScript transpiler.

### Run-time

* [Angular2](https://github.com/angular/angular): Main project framework
* [traceur-runtime](https://github.com/google/traceur-compiler): traceur utils and ES6 polyfill
* [systemjs](https://github.com/systemjs/systemjs): ES6 modules loading (module loader polyfill)
