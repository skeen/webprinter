import {
  Component,
  View,
  Host
  } from 'angular2/angular2';

@Component({
  selector: 'collapse',
})
@View({
  template: `
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

      <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Simple collapsible</button>
      <div id="demo" class="collapse">
        <ng-content></ng-content>
      </div>
  `
})
export class Collapse 
{
//    container_id:String = "demo";
//    container_id_hash:String = "#" + this.container_id;

    constructor() 
    {
    }
}
