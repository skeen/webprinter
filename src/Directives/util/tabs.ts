import {
  Component,
  View,
  NgFor,
  Host
  } from 'angular2/angular2';

@Component({
  selector: 'tabs',
})
@View({
  template: `
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <ul class="nav nav-tabs">
      <li *ng-for="#tab of tabs">
        <a data-toggle="tab" (click)="selectTab(tab)">
            {{tab.tabTitle}}
        </a>
      </li>
    </ul>
    <content></content>
  `,
  directives: [NgFor]
})
export class Tabs {
  tabs: Array<Tab>;

  constructor() {
    this.tabs = [];
  }

  selectTab(tab: Tab) {
    this.tabs.forEach((tab) => {
      tab.active = false;
    });
    tab.active = true;
  }

  addTab(tab: Tab) {
    if (this.tabs.length === 0) {
      tab.active = true;
      }
      this.tabs.push(tab);
  }
}

@Component({
  selector: 'tab',
  properties: ['tabTitle: tab-title']
})
@View({
  template: `
    <div [hidden]="!active">
      <content></content>
    </div>
  `
})
export class Tab {
    active: boolean;

    constructor(@Host() tabs:Tabs) 
    {
        tabs.addTab(this);
    }
}
