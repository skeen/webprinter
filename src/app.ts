/// <reference path="Directives/util/tabs.ts" />
/// <reference path="Directives/util/collapse.ts" />
/// <reference path="Directives/jog/JogPanel.ts" />

import {Component, View, bootstrap} from 'angular2/angular2';
import {HTTP_BINDINGS} from 'angular2/http';

import {Tabs, Tab} from './Directives/util/tabs';
import {JogPanel} from './Directives/jog/JogPanel';
import {Collapse} from './Directives/util/collapse';

// Annotation section
@Component({
    selector: 'app'
})
@View({
    template: `
    <collapse>TRY TO COLLAPSE THIS</collapse>
    `,
    directives: [Tabs, Tab, JogPanel, Collapse]
})
// Component controller
class MyAppComponent { }

bootstrap(MyAppComponent, [HTTP_BINDINGS]);
