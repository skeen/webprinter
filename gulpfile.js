var gulp = require('gulp');

var PATHS = {
    build: 'build',
    src: {
        js: 'src/**/*.ts',
        html: 'src/**/*.html',
        res: 'res/**/*'
    },
    lib: [
        'node_modules/angular2/node_modules/traceur/bin/traceur-runtime.js',
        'node_modules/angular2/bundles/angular2.dev.js',
        'node_modules/angular2/bundles/http.dev.js',
        'node_modules/systemjs/dist/system-csp-production.js'
    ],
    ng2Typing:'node_modules/angular2/bundles/typings/angular2/angular2.d.ts',
    ng2HttpTyping : 'node_modules/angular2/bundles/typings/angular2/http.d.ts'
};

gulp.task('clean', function (done) {
    var del = require('del');
    del(PATHS.build, done);
});

gulp.task('js', function () {
    var typescript = require('gulp-typescript');
    var tsResult = gulp.src([PATHS.src.js, PATHS.ng2Typing, PATHS.ng2HttpTyping])
        .pipe(typescript({
            noImplicitAny: true,
            module: 'system',
            target: 'ES5',
            emitDecoratorMetadata: true,
            experimentalDecorators: true
        }));
    return tsResult.js.pipe(gulp.dest(PATHS.build));
});

gulp.task('html', function () {
    return gulp.src(PATHS.src.html).pipe(gulp.dest(PATHS.build));
});

gulp.task('res', function () {
    return gulp.src(PATHS.src.res).pipe(gulp.dest(PATHS.build));
});

gulp.task('libs', function () {
    return gulp.src(PATHS.lib).pipe(gulp.dest(PATHS.build + '/lib'));
});

gulp.task('play', ['libs', 'html', 'js', 'res'], function () {
    var http = require('http');
    var connect = require('connect');
    var serveStatic = require('serve-static');
    var finalhandler = require('finalhandler')
    var open = require('open');
    var url = require('url');
    var cgi = require('cgi');
    var fs = require('fs');

    var port = 9000;

    gulp.watch(PATHS.src.html, ['html']);
    gulp.watch(PATHS.src.js, ['js']);
    gulp.watch(PATHS.src.res, ['res']);

    var serve = serveStatic(__dirname + '/' + PATHS.build);
    var app = connect().use(function(req, res) {
        var path = url.parse(req.url).pathname;
        // Handle CGI
        if(path.substring(0,9) == "/cgi-bin/") 
        {
            var script = __dirname + '/' + PATHS.build + path;
            try 
            {
                var stats = fs.statSync(script);

                if(stats.isDirectory())
                {
                    res.writeHead(500, {"Content-Type": "text/plain"});
                    res.write("500 Internal Server Error\n");
                    res.end();
                }
                else
                {
                    cgi(script)(req, res);
                }
            }
            catch(e)
            {
                res.writeHead(404, {"Content-Type": "text/plain"});
                res.write("404 Not Found\n");
                res.end();
            }
        }
        else // Serve static files
        {
            var done = finalhandler(req, res)
            serve(req, res, done)
        }
    });
    http.createServer(app).listen(port, function () {
        open('http://localhost:' + port);
    });
});

