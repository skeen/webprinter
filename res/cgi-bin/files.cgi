#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$DIR/json.sh"

echo "Content-Type: application/json"
echo

FILES=$(find $DIR/sd_mount -type f | xargs realpath --relative-to $DIR/sd_mount)

json_array "path" $FILES
