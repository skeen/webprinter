#!/bin/bash

function json_array 
{
    KEY=$1
    shift

    INPUT_STRING=$@
    ARRAY=(${INPUT_STRING})
    #echo ${ARRAY[@]}

    NUM_OBJECTS=${#ARRAY[@]}
    #echo $NUM_OBJECTS

    echo "["
    for i in `seq 2 $NUM_OBJECTS`;
    do
        ID=$(($i - 2))
        OBJECT=${ARRAY[$ID]}
        echo "{\"$KEY\":\"$OBJECT\"},"
    done    
    echo "{\"$KEY\":\"${ARRAY[$(($NUM_OBJECTS - 1))]}\"}"
    echo "]"
}

